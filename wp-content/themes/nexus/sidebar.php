<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nexus
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="sidebar">
	<div class="pesquisa">
		<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<input type="text" name="s" placeholder="Pesquise..." autofocus="">
			<input type="submit" name="pesquisar" value="Pesquisar">
		</form>
	</div>

	<div class="categoriasSidebar">
		<ul>
			<?php
				$i =0;
				$categorias = get_categories();
				foreach ($categorias as $categorias):
					$nomeCategoria = $categorias->name;
					$linkCategoria = get_category_link( $categorias->cat_ID );
					$bannerCategoria = z_taxonomy_image_url($categorias->cat_ID);
					if($i <5):
			?>
			<li style="background:url(<?php echo $bannerCategoria; ?>);">
				<a href="<?php echo $linkCategoria; ?>"><?php echo $nomeCategoria; ?></a>
			</li>
		<?php endif;	$i++; endforeach; ?>
		</ul>
	</div>

	<div class="categoriasSidebar">
			<p>Curta nossa página</p>
			<div class="iframeFB">
				<div class="fb-page" data-href="https://www.facebook.com/nexusguarapuava" data-tabs="timeline" data-height="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/nexusguarapuava" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/nexusguarapuava">Nexus - Administração de Condomínios</a></blockquote></div>
			</div>
	</div>

	<div class="categoriasSidebar">
		<p>Siga-nos no instagram</p>
		<div id="instafeed"></div>
	</div>
</div>
