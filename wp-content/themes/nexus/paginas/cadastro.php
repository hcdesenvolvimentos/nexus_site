<?php
/**
 * Template Name: Cadastro
 * Description: Página de Cadastro
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>
<div class="pg-cadastro">
  <div class="bannerInicial" style="background:url(<?php echo $configuracao['pg_cadastro_inicial_banner']['url']; ?>)">
    <div class="textoBanner">
      <span>Faça seu cadastro</span>
      <h2><?php echo $configuracao['pg_cadastro_inicial_titulo']; ?></h2>
    </div>
  </div>

  <section class="cadastro">
    <div class="containerFull">
      <h6 class="hidden">Cadastro</h6>

      <form action="#">
      <fieldset>
        <legend>Dados Pessoais</legend>
        <div class="iconeForm">
          <img src="<?php echo get_template_directory_uri() ?>/img/dados-pessoais-icone.png" alt="Dados Pessoais">
        </div>
        <div class="camposForm">
          <span class="maskInput user">
            <input type="text" name="nome" placeholder="Nome *">
          </span>
          <span class="maskInput rg">
            <input type="text" name="rg" placeholder="RG *">
          </span>
          <span class="maskInput">
            <img src="<?php echo get_template_directory_uri() ?>/img/cpf-icone.png" alt="CPF">
            <input type="text" name="cpf" placeholder="CPF *">
          </span>
        </div>
      </fieldset>

      <fieldset>
        <legend>Contato</legend>
        <div class="iconeForm">
          <img src="<?php echo get_template_directory_uri() ?>/img/contato-icone.png" alt="Contato">
        </div>
        <div class="camposForm">
          <span class="maskInput mail">
            <input type="email" name="email" placeholder="E-mail *">
          </span>
          <span class="maskInput phone">
            <input type="text" name="celular" placeholder="Celular *">
          </span>
          <span class="maskInput phone">
            <input type="text" name="telefone" placeholder="Telefone *">
          </span>
        </div>
      </fieldset>

      <fieldset>
        <legend>Endereço</legend>
        <div class="iconeForm">
          <img src="<?php echo get_template_directory_uri() ?>/img/endereco-icone.png" alt="Endereço">
        </div>
        <div class="camposForm">
          <span class="maskInput building">
            <input type="text" name="condominio" placeholder="Condomínio *">
          </span>
          <span class="maskInput building">
            <input type="text" name="apartamento" placeholder="Apartamento *">
          </span>
          <span class="maskInput map">
            <input type="text" name="endereco" placeholder="Endereço *">
          </span>
        </div>
      </fieldset>

      <fieldset>
        <legend>Tipo de cadastro</legend>
        <div class="camposForm radios">
          <label for="proprietario"><input type="radio" id="proprietario" required> Proprietário</label>
          <label for="inquilino"><input type="radio" id="inquilino" required> Inquilino</label>
          <label for="morador"><input type="radio" id="morador" required> Morador</label>
        </div>
      </fieldset>

      <input type="submit" value="Enviar">
    </form>
    </div>
  </section>
</div>
<?php get_footer(); ?>
