<?php
/**
 * Template Name: Agendamento de Mudança
 * Description: Página de Agendamento de Mudança
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>
<div class="pg-agendamento-mudanca">
  <div class="bannerInicial" style="<?php echo "background:url(". $configuracao['pg_agendamento_inicial_banner']['url'] . ")"; ?>">
    <div class="textoBanner">
      <span>Agendamento de mudança</span>
      <h2><?php echo $configuracao['pg_agendamento_inicial_titulo']; ?></h2>
    </div>
  </div>

  <div class="containerFull">
    <form action="#">
      <fieldset>
        <legend>Contato</legend>
        <div class="iconeForm">
          <img src="<?php echo get_template_directory_uri() ?>/img/contato-icone.png" alt="Contato">
        </div>
        <div class="camposForm">
          <span class="maskInput user">
            <input type="text" name="nome" placeholder="Nome *">
          </span>
          <span class="maskInput mail">
            <input type="email" name="email" placeholder="E-mail *">
          </span>
          <span class="maskInput phone">
            <input type="text" name="telefone" placeholder="Telefone *">
          </span>
        </div>
      </fieldset>

      <fieldset>
        <legend>Mudança</legend>
        <div class="iconeForm">
          <img src="<?php echo get_template_directory_uri() ?>/img/mudanca-icone.png" alt="">
        </div>
        <div class="camposForm">
          <span class="maskInput building">
            <input type="text" name="condominio" placeholder="Condomínio *">
          </span>
          <span class="maskInput building">
            <input type="text" name="apartamento" placeholder="Apartamento *">
          </span>
          <span class="maskInput calendar">
            <input type="date" name="data-mudanca" placeholder="Data de mudança *">
          </span>
        </div>
      </fieldset>

      <fieldset>
        <legend>Tipo de mudança</legend>
        <div class="camposForm radios">
          <label for="entrada"><input type="radio" id="entrada"> Entrada</label>
          <label for="saida"><input type="radio" id="saida"> Saída</label>
        </div>
      </fieldset>

      <input type="submit" value="Enviar">
    </form>
  </div>
</div>
<?php get_footer(); ?>
