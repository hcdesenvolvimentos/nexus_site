<?php
/**
 * Template Name: Contato
 * Description: Página de Contato
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>
<div class="pg-contato">
  <div class="bannerContato" style="<?php echo "background:url(". $configuracao['pg_contato_inicial_banner']['url'] . ")"; ?>">
    <div class="conteudoBanner">
      <span>Contato</span>
      <h4><?php echo $configuracao['pg_contato_inicial_titulo']; ?></h4>
    </div>
  </div>

  <div class="formularioContato">
    <div class="containerFull">
      <div class="row">
        <div class="col-md-6">
          <p>Formulário de contato</p>
          <form action="#">
            <span class="maskInput user">
              <input type="text" name="nome" placeholder="Nome*">
            </span>
            <span class="maskInput mail">
              <input type="email" name="email" placeholder="Email*">
            </span>
            <span class="maskInput message">
              <textarea name="mensagem" id="mensagem" placeholder="Mensagem*"></textarea>
            </span>
            <span class="maskInput condominio">
              <input type="text" name="condominio" placeholder="Condomínio">
            </span>
            <input type="submit" value="Enviar">
          </form>
        </div>
        <div class="col-md-6">
          <div class="redesSociaisContato">
            <ul>
              <?php if($configuracao['config_redes_sociais_contato_telefone']):
                $telefone = $configuracao['config_redes_sociais_contato_telefone'];
                $subString = "|";
                $pos = strpos($telefone, $subString);
                if(!($pos === false)){
                  $telefones = explode('|',$telefone);
                }

              ?>
              <li>
                <a href="tel:<?php echo $telefones[0]; ?>">
                  <i class="fas fa-phone"></i>
                  <span><?php echo $telefones[0]; ?></span>
                  <span><?php echo $telefones[1]; ?></span>
                </a>
              </li>
            <?php endif; if($configuracao['config_redes_sociais_instagram']):?>
              <li>
                <a href="<?php echo $configuracao['config_redes_sociais_instagram']; ?>" target="_blank">
                  <i class="fab fa-instagram"></i>
                  <span>@nexusguarapuava</span>
                </a>
              </li>
            <?php endif; if($configuracao['config_redes_sociais_facebook']): ?>
              <li>
                <a href="<?php echo $configuracao['config_redes_sociais_facebook']; ?>" target="_blank">
                  <i class="fab fa-facebook-f"></i>
                  <span>/nexusguarapuava</span>
                </a>
              </li>
            <?php endif; if($configuracao['config_redes_sociais_contato_email']): ?>
              <li>
                <a href="<?php echo "mailto:". $configuracao['config_redes_sociais_contato_email']; ?>">
                  <i class="far fa-envelope"></i>
                  <span><?php echo $configuracao['config_redes_sociais_contato_email']; ?></span>
                </a>
              </li>
            <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
