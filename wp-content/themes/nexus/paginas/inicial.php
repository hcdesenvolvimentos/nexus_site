<?php
/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>
<!-- PG INICIAL -->
<div class="pg pg-inicial">
  <section class="sessaoInicial" style="background: url(<?php echo $configuracao['pg_inicial_banner_img']['url']; ?>)">
    <div class="conteudo">
      <h3 class="titulo"><?php echo $configuracao['pg_inicial_banner_titulo_banner']; ?></h3>
      <h6 class="subtitulo"><?php echo $configuracao['pg_inicial_banner_subtitulo']; ?></h6>
      <div class="botoes">
        <a href="#">Nossos Serviços</a>
        <a href="#" class="orcamento">Solicite uma proposta</a>
      </div>
      <span class="aviso"><?php echo $configuracao['pg_inicial_banner_condicoes']; ?></span>
    </div>
  </section>

  <section class="vantagens relativeClass">
    <div class="containerFull">
      <div class="row">
        <div class="col-sm-5">
          <figure>
            <img src="<?php echo $configuracao['pg_inicial_vantagens_img']['url'] ?>" alt="Vantagens">
          </figure>
        </div>
        <div class="col-sm-7">
          <div class="textoVantagens">
            <h3 class="titulo"><?php echo $configuracao['pg_inicial_vantagens_titulo']; ?></h3>
            <?php echo $configuracao['pg_inicial_vantagens_texto']; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="skewLeft"></div>
    <div class="skewRight"></div>
  </section>

  <section class="servicos relativeClass">


    <h2 class="tituloServicos">Nossos serviços</h2>
    <div class="containerFull">
      <div class="listaServicos">
      <?php $postServicos = new WP_Query( array( 'post_type' => 'servicos', 'posts_per_page' => -1 ) ); ?>
      <ul>

        <?php  while ($postServicos->have_posts()):
          $postServicos->the_post();
          $imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
          $imagemDestaque = $imagemDestaque[0];
        ?>
        <li>
          <div class="imagemServico">
            <figure>
              <img src="<?php echo $imagemDestaque; ?>" alt="<?php echo $titulo; ?>">
            </figure>
          </div>
          <div class="textoServico">
            <h3><?php echo get_the_title(); ?></h3>
            <?php echo the_content(); ?>
          </div>
        </li>
      <?php endwhile; wp_reset_query();?>
      </ul>
    </div>
    </div>
  </section>

  <section class="portalCondomino relativeClass">
    <div class="containerFull">
      <div class="row">
        <div class="col-sm-6">
          <div class="textoPortal">
            <h4><?php echo $configuracao['pg_inicial_portal_titulo']; ?></h4>
            <?php echo $configuracao['pg_inicial_portal_conteudo']; ?>
            <a href="#"> Acessar o portal</a>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="carrosselNexus">
            <div class="mockup">
              <button class="esquerdaCarrossel"><i class="fas fa-chevron-left"></i></button>
              <button class="direitaCarrossel"><i class="fas fa-chevron-right"></i></button>
            </div>
            <div class="carrosselPortal" id="carrosselPortal">
              <?php
              $imagensCarrossel = explode(',', $configuracao['pg_inicial_portal_carrossel']);

              foreach ($imagensCarrossel as $imagensCarrossel ):
                $imagem = wp_get_attachment_url($imagensCarrossel);
                ?>
              <div class="item">
                <figure>
                  <img src="<?php echo $imagem; ?>" alt="Item">
                </figure>
              </div>
            <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if($configuracao['pg_inicial_estatísticas_item1']): ?>
    <section class="estatisticas relativeClass">
      <h5>Estatísticas</h5>
      <div class="estatisticasNexus">
        <ul>
          <li>
            <?php
              $item1 = explode('|',$configuracao['pg_inicial_estatísticas_item1']);

            ?>
            <span class="numero more"><?php echo $item1[0]; ?></span>
            <p class="descricao"><?php echo $item1[1]; ?></p>
          </li>
          <li>
            <?php
              $item2 = explode('|',$configuracao['pg_inicial_estatísticas_item2']);
            ?>
            <span class="numero"><?php echo $item2[0]; ?></span>
            <p class="descricao"><?php echo $item2[1]; ?></p>
          </li>
          <li>
            <?php
              $item3 = explode('|',$configuracao['pg_inicial_estatísticas_item3']);
            ?>
            <span class="numero percent"><?php echo $item3[0]; ?></span>
            <p class="descricao"><?php echo $item3[1]; ?></p>
          </li>
          <li>
            <?php
              $item4 = explode('|',$configuracao['pg_inicial_estatísticas_item4']);
            ?>
            <span class="numero percent less"><?php echo $item4[0]; ?></span>
            <p class="descricao"><?php echo $item4[1]; ?></p>
          </li>
        </ul>
      </div>
    </section>
  <?php endif; ?>


  <?php $noticias = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3 ) );
   if($noticias->have_posts()): ?>
    <section class="noticias relativeClass">
      <div class="containerFull">
        <h5>Notícias</h5>
        <div class="postsBlogs">
          <ul class="listaDePost">
            <?php while($noticias->have_posts()):
              $noticias->the_post();
              $imagemDestaqueNoticia = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
              $imagemDestaqueNoticia = $imagemDestaqueNoticia[0];
            ?>
            <li class="post">
              <a href="<?php echo get_permalink(); ?>">
                <h2 class="tituloPost"><?php echo get_the_title(); ?></h2>
                <article>
                  <figure class="imagemDestaque" style="background: url(<?php echo $imagemDestaqueNoticia ?>)">
                    <img src="<?php echo $imagemDestaqueNoticia ?>" alt="<?php echo get_the_title(); ?>">
                  </figure>
                  <span class="saibaMais">Saiba Mais</span>
                  <p class="descricaoPost"><?php echo customExcerpt(170); ?></p>
                </article>
              </a>
            </li>
          <?php endwhile; wp_reset_query(); ?>
          </ul>
        </div>
      </section>
    <?php endif; ?>
  </div>
</div>

<?php get_footer(); ?>
