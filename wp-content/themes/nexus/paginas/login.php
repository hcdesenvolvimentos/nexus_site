<?php
/**
 * Template Name: Login
 * Description: Página de Login
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>
  <style>
    header{
     display: none;
    }
  </style>
  <div class="pg-login">
  <div class="row">
    <div class="col-md-8">
      <div class="bannerLogin">
        <div class="conteudoTexto">
          <span>Bem vindo ao</span>
          <strong>Portal do Condômino</strong>
          <a href="#">Voltar para o site</a>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="formLogin">
        <div class="conteudoLogin">
          <div class="logo">
            <figure>
              <img src="<?php echo get_template_directory_uri() ?>/img/logoLogin.png" alt="Nexus">
            </figure>
          </div>
          <form action="http://www.icondev.com.br/IconPortalCondomino/site/inicio.jsp" method="post">
            <span class="inputText">
              <input type="text" name="usuario" placeholder="Usuário">
            </span>
            <span class="inputPass">
              <input type="password" name="senha" placeholder="Senha">
            </span>
            <a href="#">Esqueci minha senha</a>
            <button type="submit"><i class="fas fa-user"></i> Acessar Portal</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
