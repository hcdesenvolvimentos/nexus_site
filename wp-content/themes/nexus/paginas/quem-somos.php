<?php
/**
 * Template Name: Quem Somos
 * Description: Página Quem Somos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */
get_header();
?>

  <div class="pg-quem-somos">
    <section class="inicial" style="background:url(<?php echo $configuracao['pg_quem_somos_banner_img']['url']; ?>)">
      <div class="containerFull">
        <h6 class="hidden">Quem somos</h6>
        <div class="textoBanner">
          <p>Quem somos</p>
          <h2><?php echo $configuracao['pg_quem_somos_titulo']; ?></h2>
        </div>
      </div>
    </section>

    <section class="sobreNexus">
      <div class="containerFull">
        <div class="textoSobre">
          <h4 class="titulo"><?php echo $configuracao['pg_quem_somos_sobre_titulo']; ?></h4>
          <?php echo $configuracao['pg_quem_somos_sobre_conteudo']; ?>
        </div>
        <div class="redesSociais">
          <ul>
          <?php if($configuracao['config_redes_sociais_facebook']): ?>
            <li>
              <a href="<?php echo $configuracao['config_redes_sociais_facebook']; ?>">
                <i class="fab fa-facebook-f"></i>
                <span>Facebook</span>
                <p>Acompanhe nossa página</p>
              </a>
            </li>
          <?php endif; if($configuracao['config_redes_sociais_instagram']):?>
            <li>
              <a href="<?php echo $configuracao['config_redes_sociais_instagram']; ?>">
                <i class="fab fa-instagram"></i>
                <span>Instagram</span>
                <p>Fique de olho no nosso perfil</p>
              </a>
            </li>
          <?php endif; ?>
            <li>
              <a href="<?php echo get_home_url(); ?>/contato">
                <i class="fas fa-comments"></i>
                <span>Fale conosco</span>
                <p>Envie-nos uma mensagem</p>
              </a>
            </li>
            <li>
              <a href="<?php echo get_home_url(); ?>/portal-condomino">
                <i class="fas fa-user"></i>
                <span>Portal</span>
                <p>Extratos, boletos, agendamentos...</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <div class="gMaps">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.3879355112!2d-51.47378338437311!3d-25.391821937605393!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef363317e874ef%3A0x357390e38ed19e18!2sR.+Pres.+Get%C3%BAlio+Vargas%2C+1941+-+Centro%2C+Guarapuava+-+PR%2C+85010-280!5e0!3m2!1spt-BR!2sbr!4v1544102782153" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>

<?php get_footer(); ?>
