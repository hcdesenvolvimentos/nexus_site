<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nexus
 */

get_header();
?>
		<?php if ( have_posts() ) : ?>
					<div class="resultadoBusca">
						<p><?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Resultados encontrados para: %s', 'nexus' ), '<span>' . get_search_query() . '</span>' );
						?></p>
					</div>
					<div class="containerFull">
						<div class="row">
							<div class="col-md-9">
								<div class="posts">
									<ul class="listaDePost">
									<?php while(have_posts()):
										the_post();
										$imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
										$imagemPost = $imagemPost[0];
										global $post;
										if($post->post_type == 'post'):
									?>
										<li class="post">
											<a href="<?php echo get_permalink(); ?>">
												<h2 class="tituloPost"><?php echo get_the_title(); ?></h2>
												<article>
													<figure class="imagemDestaque" style="background: url(<?php echo $imagemPost; ?>);">
														<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
													</figure>
													<span class="saibaMais">Saiba Mais</span>
													<p class="descricaoPost"><?php customExcerpt(170); ?></p>
												</article>
											</a>
										</li>
									<?php endif; endwhile; ?>
									</ul>
								</div>
							</div>
							<div class="col-md-3">
								<?php get_sidebar(); ?>
							</div>
						</div>
					</div>
		<?php	else: ?>
			<div class="resultadoBusca">
				<p><?php
				printf( esc_html__( 'Nenhum resultado encontrado para: %s', 'nexus' ), '<span>' . get_search_query() . '</span>' );
				?></p>
			</div>
		<?php	endif; ?>
<?php
get_footer();
