<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nexus
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2&appId=1824562047656616&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<!-- TOPO -->
		<header class="topo">
		<div class="row">
			<div class="menuCima">
				<div class="containerFull">
					<div class="row">
						<div class="col-sm-9">
							<ul>
								<li><a href="https://www.google.com.br/maps/place/<?php echo $configuracao['config_endereco_endereco']; ?>"><span>Localização</span> <?php echo $configuracao['config_endereco_endereco']; ?></a></li>

								<?php if($configuracao['config_redes_sociais_contato_telefone']):
	                $telefone = $configuracao['config_redes_sociais_contato_telefone'];
	                $subString = "|";
	                $pos = strpos($telefone, $subString);
	                if(!($pos === false)){
	                  $telefones = explode('|',$telefone);
	                }

	              ?>
									<li><a href="<?php echo "tel:". $telefones[0]; ?>"><span>Telefone</span><?php echo $telefones[0]; ?></a></li>
								<?php endif; ?>
							</ul>
						</div>
						<div class="col-sm-3">
							<div class="redesSociaisCima">
								<ul>
									<!-- <li><a href="#"><i class="fab fa-twitter"></i></a></li> -->
									<?php if($configuracao['config_redes_sociais_instagram']): ?>
										<li><a href="<?php echo $configuracao['config_redes_sociais_instagram']; ?>"><i class="fab fa-instagram"></i></a></li>
									<?php endif;
									if($configuracao['config_redes_sociais_facebook']):
										?>
										<li><a href="<?php echo $configuracao['config_redes_sociais_facebook']; ?>"><i class="fab fa-facebook-f"></i></a></li>
									<?php endif;
									if($configuracao['config_redes_sociais_whatsapp']):
										?>
										<li><a href="<?php echo $configuracao['config_redes_sociais_whatsapp']; ?>"><i class="fab fa-whatsapp"></i></a></li>
									<?php endif;
									if($configuracao['config_redes_sociais_linkedin']):
										?>
										<li><a href="<?php echo $configuracao['config_redes_sociais_linkedin']; ?>"><i class="fab fa-linkedin-in"></i></a></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="containerFull">
			<div class="row">
				<!-- LOGO -->
				<div class="col-sm-2">
					<a href="<?php echo get_home_url(); ?>">
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="Nexus">
					</a>
				</div>
				<!-- MENU  -->
				<div class="col-sm-10">
					<div class="navbar" role="navigation">
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!--  MENU MOBILE-->
						<div class="row navbar-header">
							<nav class="collapse navbar-collapse" id="collapse">
									<?php
										$menu = array(
											'theme_location'  => '',
											'menu'            => 'Menu Principal',
											'container'       => false,
											'container_class' => '',
											'container_id'    => '',
											'menu_class'      => 'nav navbar-nav',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth'           => 2,
											'walker'          => ''
										);
										wp_nav_menu( $menu );
									?>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
