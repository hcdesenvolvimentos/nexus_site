<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nexus
 */
 $imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
 $imagemPost = $imagemPost[0];
global $post;

$categoria = get_the_category($post->ID);
$nomeCategoria = $categoria->name;
$linkCategoria = get_category_link( $categoria->term_id );
get_header();
?>

<div class="pg-post">
	<div class="bannerInicialPost"></div>
	<div class="containerFull">
		<div class="breadcrumbs">
			<ul>
				<?php
					$categoria = get_the_category($post->ID);
					foreach ($categoria as $categoria) {
						$nomeCategoria = $categoria->name;
						$linkCategoria = get_category_link( $categoria->cat_ID );
					}
				?>
				<li>
					<a href="<?php echo get_home_url(); ?>/noticias">Blog da Nexus</a>
				</li>
				<li>
					<a href="<?php echo $linkCategoria; ?>"><?php echo $nomeCategoria; ?></a>
				</li>
				<li class="ativo">
					<a href="#"><?php echo get_the_title(); ?></a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="informacoesPost">
					<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
					<span class="dataPost"><?php the_time('j/m/Y') ?></span>
					<figure class="imagemDestaquePost" style="background:url(<?php echo $imagemPost; ?>);">
						<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
					</figure>
				</div>
				<div class="conteudoPost">
					<?php
						while(have_posts()){
							the_post();
							the_content();
						}
						?>
				</div>
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
