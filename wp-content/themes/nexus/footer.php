<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nexus
 */

?>

<!-- RODAPÉ -->
<footer class="rodape relativeClass">
	<div class="containerFull">
		<div class="row">
			<div class="col-md-2">
				<div class="logoRodape">
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="Nexus">
					</a>
				</div>
			</div>
			<div class="col-md-5 col-lg-2">
				<div class="textoRodape">
					<h5>Informações</h5>

					<ul>
						<li><a href="#">Rua Getúlio Vargas, 1941, Centro Guarapuava Paraná</a></li>
						<li><a href="#">contato@nexus.com</a></li>
						<li><a href="#">(42) 3632-4444</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-5 col-lg-2">
				<div class="textoRodape">
					<h5>Acesso Rápido</h5>
						<?php
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu Rodapé',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
							);
							wp_nav_menu( $menu );
						?>
				</div>
			</div>
			<div class="col-lg-2 pgFb">
				<div class="textoRodape instagram">
					<h5>Instagram</h5>

					<img src="<?php echo get_template_directory_uri(); ?>/img/insta.png" alt="" style="width: 100; max-width: 180px;">
				</div>
			</div>
			<div class="col-lg-3 pgFb">
				<div class="pgFacebook">
					<div class="fb-page" data-href="https://www.facebook.com/nexusguarapuava" data-tabs="timeline" data-height="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/nexusguarapuava" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/nexusguarapuava">Nexus - Administração de Condomínios</a></blockquote></div>
				</div>
			</div>
		</div>

		<div class="copyright">
			<div class="row">
				<div class="col-xs-6">
					<p>Todos os direitos reservados - CNPJ 00100123456</p>
				</div>
				<div class="col-xs-6">
					<div class="redesSociaisRodape">
						<ul>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-instagram"></i></a></li>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
