<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */

get_header();
?>
<div class="pg-blog">
	<div class="bannerInicial">
		<div class="textoBanner">
			<span>Blog da Nexus</span>
			<h2>Conheça <strong>todas</strong> as <strong>dicas</strong> e <strong>curiosidades</strong></h2>
		</div>
	</div>
	<section class="ultimosPosts">
		<h6 class="hidden">Ultimos posts</h6>
		<div class="containerFull">
			<div class="cabecalhoPosts">
				<div class="row">
					<div class="col-sm-9">
						<p>Últimos posts</p>
					</div>
					<div class="col-sm-3">
						<div class="filtro">

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-9">
					<div class="posts">
						<ul class="listaDePost">
						<?php while(have_posts()):
							the_post();
							$imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							$imagemPost = $imagemPost[0];
						?>
							<li class="post">
								<a href="<?php echo get_permalink(); ?>">
									<h2 class="tituloPost"><?php echo get_the_title(); ?></h2>
									<article>
										<figure class="imagemDestaque" style="background: url(<?php echo $imagemPost; ?>);">
											<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
										</figure>
										<span class="saibaMais">Saiba Mais</span>
										<p class="descricaoPost"><?php customExcerpt(170); ?></p>
									</article>
								</a>
							</li>
						<?php endwhile; ?>
						</ul>
						<div class="verMais">
							<a href="">Ver mais</a>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
get_footer();
