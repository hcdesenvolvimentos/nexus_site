<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nexus
 */

get_header();
?>
		<div class="containerFull">
			<div class="row">
				<div class="col-md-9">
					<div class="posts">
						<ul class="listaDePost">
						<?php while(have_posts()):
							the_post();
							$imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							$imagemPost = $imagemPost[0];
						?>
							<li class="post">
								<a href="<?php echo get_permalink(); ?>">
									<h2 class="tituloPost"><?php echo get_the_title(); ?></h2>
									<article>
										<figure class="imagemDestaque" style="background: url(<?php echo $imagemPost; ?>);">
											<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
										</figure>
										<span class="saibaMais">Saiba Mais</span>
										<p class="descricaoPost"><?php customExcerpt(170); ?></p>
									</article>
								</a>
							</li>
						<?php endwhile; ?>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
<?php
get_footer();
