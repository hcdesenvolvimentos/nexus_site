<?php

/**
 * Plugin Name: Base Nome do Projeto
 * Description: Controle base do tema Nome do Projeto.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseProjeto () {

		// TIPOS DE CONTEÚDO
		conteudosProjeto();

		taxonomiaProjeto();

		metaboxesProjeto();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosProjeto (){

		// TIPOS DE CONTEÚDO
		//tipoDestaque();
		tipoServicos();




		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	function tipoServicos() {

		$rotulosServicos = array(
			'name'               => 'Serviços',
			'singular_name'      => 'serviço',
			'menu_name'          => 'Serviços',
			'name_admin_bar'     => 'Serviços',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo serviço',
			'new_item'           => 'Novo serviço',
			'edit_item'          => 'Editar serviço',
			'view_item'          => 'Ver serviço',
			'all_items'          => 'Todos os serviços',
			'search_items'       => 'Buscar serviço',
			'parent_item_colon'  => 'Dos serviços',
			'not_found'          => 'Nenhum serviço cadastrado.',
			'not_found_in_trash' => 'Nenhum serviço na lixeira.'
		);

		$argsServicos 	= array(
			'labels'             => $rotulosServicos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-hammer',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'servicos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'show_in_rest' => true,
			//faz do guttenberg o editor padrao
			'supports'           => array( 'title','editor','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servicos', $argsServicos);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaProjeto () {
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesProjeto(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Projeto_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque-inicial' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto Mobile: ',
						'id'    => "{$prefix}foto_mob",
						'desc'  => 'Foto mobile 320 X 480',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  => 'Vídeo: ',
						'id'    => "{$prefix}checkbox_destaqueinicial",
						'desc'  => 'Destaque do tipo vídeo marque aqui!',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Imagem: ',
						'id'    => "{$prefix}checkbox_imagem",
						'desc'  => 'Marque está opção, se for arte pronta',
						'type'  => 'checkbox'
					),
					array(
						'name'  => 'Vídeo URL: ',
						'id'    => "{$prefix}video_destaqueinicial",
						'desc'  => 'Adicione o vídeo as mídias e adicione a url aqui!',
						'type'  => 'text'
					),

				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesProjeto(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerProjeto(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseProjeto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );
